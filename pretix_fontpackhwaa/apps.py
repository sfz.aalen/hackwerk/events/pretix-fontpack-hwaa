from django.apps import AppConfig
from . import __version__


class PluginApp(AppConfig):
    name = 'pretix_fontpackhwaa'
    verbose_name = 'Fontpack: HWAA'

    class PretixPluginMeta:
        name = 'Fontpack: Hackwerk Aalen'
        author = 'Hackwerk Aalen / Nils Dellemann, Original: Raphael Michel'
        description = 'Pack of an edited Atkinson Hyperlegible font'
        visible = False
        version = __version__
        compatibility = "pretix>=4.16.0"

    def ready(self):
        from . import signals  # NOQA

