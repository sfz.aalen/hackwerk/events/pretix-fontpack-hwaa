from django.dispatch import receiver
from django.utils.safestring import mark_safe

from pretix.plugins.ticketoutputpdf.signals import register_fonts


@receiver(register_fonts, dispatch_uid="fontpack_hwaa_fonts")
def fontpack_free(sender, **kwargs):
    basepath = 'pretix_fontpackhwaa'
    return {
        "Atkinson Hyperlegible, HWAA Edition": {
            "regular": {
                "truetype": basepath + "/Atkinson-Hyperlegible-Regular-102.ttf",
                "woff": basepath + "/Atkinson-Hyperlegible-Regular-102.woff",
                "woff2": basepath + "/Atkinson-Hyperlegible-Regular-102.woff2",
            },
            "sample": mark_safe(
               "THE MINISTRY REMINDS ALL CITIZENS THAT THEIR SUBSCRIPTIONS ARE MONITORED, and that this is all supposed to be uppercase"
            )
        }
    }
